---
wsId: maiarwallet
title: Maiar
altTitle: 
authors:
- danny
appId: com.elrond.maiar.wallet
appCountry: us
idd: 1519405832
released: 2021-01-31
updated: 2022-06-11
version: 1.4.24
stars: 4.7
reviews: 344
size: '303827968'
website: https://maiar.com/
repository: 
issue: 
icon: com.elrond.maiar.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-05
signer: 
reviewArchive: 
twitter: getMaiar
social:
- https://www.linkedin.com/company/getmaiar
- https://www.facebook.com/getMaiar

---

{% include copyFromAndroid.html %}
