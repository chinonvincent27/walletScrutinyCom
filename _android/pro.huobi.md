---
wsId: huobi
title: 'Huobi: Trade Crypto & Bitcoin'
altTitle: 
authors:
- leo
users: 5000000
appId: pro.huobi
appCountry: 
released: 2017-11-01
updated: 2022-06-27
version: 7.0.8
stars: 4.8
ratings: 31840
reviews: 536
size: 
website: http://www.hbg.com
repository: 
issue: 
icon: pro.huobi.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: HuobiGlobal
social:
- https://www.facebook.com/huobiglobalofficial
redirect_from:
- /pro.huobi/
- /posts/pro.huobi/

---

Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
