---
wsId: 
title: 'monerujo: Monero Wallet'
altTitle: 
authors:
- leo
users: 50000
appId: com.m2049r.xmrwallet
appCountry: 
released: 2017-09-29
updated: 2022-05-23
version: 2.4.3 'Baldaŭ'
stars: 3.1
ratings: 821
reviews: 160
size: 
website: https://monerujo.io/
repository: 
issue: 
icon: com.m2049r.xmrwallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-02-27
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

This app does not feature BTC wallet functionality.