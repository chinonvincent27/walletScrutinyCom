---
wsId: 
title: SecuX Firmware Update
altTitle: 
authors: 
users: 1000
appId: com.secux.firmwareupdate
appCountry: 
released: Sep 28, 2021
updated: 2022-05-07
version: 1.0.4
stars: 2.5
ratings: 
reviews: 5
size: 
website: https://www.secuxtech.com
repository: 
issue: 
icon: com.secux.firmwareupdate.png
bugbounty: 
meta: ok
verdict: wip
date: 2022-06-22
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

